# Import the libraries
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.linear_model import LogisticRegression


# Make data
centers = [[4,4], [2,2]]
X, y = make_blobs(n_samples=1000, centers=centers, center_box=[2,2])

# Create mesh grid
x_min = X[:,0].min()-1
x_max = X[:,0].max()+1
y_min = X[:,1].min()-1
y_max = X[:,1].max()+1
xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.02),
                     np.arange(y_min, y_max, 0.02))

# Create plot
plt.ion()
fig, ax = plt.subplots()

# Create model per number of iterations
for i in range(1,20):
    # Logistic regression
    clf = LogisticRegression(penalty='none', random_state = 1, max_iter=i).fit(X,y)

    # Predict the outcome of the complete meshgrid
    a = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    a = a.reshape(xx.shape)

    # Plot the contour line and update it each iteration
    # If it is the first iteration:
    if i==1:
        lijn = [ax.contour(xx, yy, a)]
        ax.scatter(X[y == 0, 0], X[y == 0, 1], c='k')
        ax.scatter(X[y == 1, 0], X[y == 1, 1], c='r')
        plt.draw()
        continue

    # For later iterations
    for l in lijn[0].collections:
        l.remove()

    lijn[0] = ax.contour(xx, yy, a)
    plt.draw()
    plt.pause(0.5)

# Show the data
plt.show()